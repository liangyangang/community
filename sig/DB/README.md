# DB SIG
English | [简体中文](./README.CN.md)

*DB SIG as a working group to introduce the whole database ecosystem into openEuler community.*<br>
*\*Notes\*: As DB SIG will work with the global contributors, so we suggest all development progress or issue description
should be in english version.*

## The major goal and working scope of DB SIG
- Be responsibility for maintaining the database open-source software packages in openEuler, including Mysql, MariaDB,
PostgreSQL and openGauss.
- Introducing more OLTP database into openEuler, such as openGauss or any other popular open-source databases into
openEuler community.
- Introducing the ecosystem tools around databases to enrich and complete the entire DB product software ecosystem.


## Members

### Maintainers
- Zhenyu Zheng[@ZhengZhenyu](https://gitee.com/ZhengZhenyu), *zheng.zhenyu@outlook.com*
- Bo Zhao[@bzhaoop](https://gitee.com/bzhaoop), *bzhaojyathousandy@gmail.com*

### Committers
- Qide Chen[@dillon_chen](https://gitee.com/dillon_chen), *dillon.chen@turbolinux.com.cn*
- Zhenyu Zheng[@ZhengZhenyu](https://gitee.com/ZhengZhenyu), *zheng.zhenyu@outlook.com*
- Bo Zhao[@bzhaoop](https://gitee.com/bzhaoop), *bzhaojyathousandy@gmail.com*


## Meeting schdule
- TBD

### Contacts
- [MailList](dev@openeuler.org) *dev@openeuler.org*
- [slack](https://join.slack.com/t/slack-jma9373/shared_invite/zt-o66x6a3a-HY4Cwjc49XPxc9aN_FHOdg)
- **wechat** *If you are interested in this SIG group, please send your personal wechat_id to Bo Zhao. He will invite you 
to DB SIG wechat group.*


# Projects

*<The project name should be the same as the application form mentioned, and the specific repository location can be
refreshed after the application is submitted.>*

Project names && repository location：
- https://gitee.com/src-openeuler/bucardo
- https://gitee.com/src-openeuler/derby
- https://gitee.com/src-openeuler/firebird
- https://gitee.com/src-openeuler/foomatic
- https://gitee.com/src-openeuler/foomatic-db
- https://gitee.com/src-openeuler/geolatte-geom
- https://gitee.com/src-openeuler/glassfish-legal
- https://gitee.com/src-openeuler/gupnp-dlna
- https://gitee.com/src-openeuler/h2
- https://gitee.com/src-openeuler/mariadb-connector-odbc
- https://gitee.com/src-openeuler/mysql5
- https://gitee.com/src-openeuler/perl-DBIx-Safe
- https://gitee.com/src-openeuler/pgadmin4-server
- https://gitee.com/src-openeuler/pgpool2
- https://gitee.com/src-openeuler/postgresql
- https://gitee.com/src-openeuler/postgresql-odbc
- https://gitee.com/src-openeuler/unixODBC
- https://gitee.com/src-openeuler/mariadb
- https://gitee.com/src-openeuler/mariadb-connector-c
- https://gitee.com/src-openeuler/sqlite
- https://gitee.com/src-openeuler/perl-DBD-SQLite
- https://gitee.com/src-openeuler/perl-DBD-MySQL
